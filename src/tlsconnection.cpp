#include "sero/tlsconnection.hpp"
#include "origo/future.hpp"
#include <talos/Tls.hpp>
#include <manto/tcp_socket.hpp>
#include <cstring>
#include <map>

namespace Sero {

tlsserver& tlsserver::Instance() {
  static tlsserver server;
  return server;
}

void tlsserver::RegisterHandler(std::string protocol, std::function<void(tls, uint16_t, std::string)> onConnect) {
  onConnects.emplace(protocol, onConnect);
}

void tlsserver::addcert(std::string_view certificate, std::string_view privatekey) {
  handle.AddIdentity(certificate, privatekey);
}

Origo::future<void> tlsserver::NewConnection(tcp_socket sock, uint16_t port, std::string ipaddr) {
  std::vector<std::string> alpnOffers;
  for (auto& [protocol, handler] : onConnects) {
    alpnOffers.push_back(protocol);
  }
  uint64_t currentTime = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
  tls client = co_await tls::createServer(handle, std::move(sock), Origo::time::from_unix(currentTime), alpnOffers);
  std::string protocol = std::string(client.GetAlpnProtocol());
  if (protocol.empty()) protocol = defaultProtocols[port];

  auto it = onConnects.find(protocol);
  if (it != onConnects.end()) {
    it->second(std::move(client), port, ipaddr);
  }
}

void tlsserver::open_port(uint16_t port, std::string_view defaultProtocol) {
  listensockets.push_back(std::make_unique<tcp_listen_socket>("0.0.0.0:" + std::to_string(port), [=,this](tcp_socket sock, std::string ipaddr) {
    NewConnection(std::move(sock), port, ipaddr);
  }));
  defaultProtocols[port] = std::string(defaultProtocol);
}

Origo::future<tls> tls::createClient(Talos::TlsContextHandle& handle, tcp_socket remote, std::string hostname, Origo::time currentTime, std::vector<std::string> requestedAlpnProtocols)
{
  tls t(handle, std::move(remote), hostname, currentTime, requestedAlpnProtocols);
  std::vector<uint8_t> toSend;
  toSend = t.state.startupExchange({});
  co_await t.sock.sendmsg(toSend);

  std::vector<uint8_t> received;
  received.reserve(8192);
  do {
    received.resize(8192);
    size_t bytesReceived = co_await t.sock.recvmsg(received.data(), 8192);
    received.resize(bytesReceived);

    std::vector<uint8_t> toSend;
    toSend = t.state.startupExchange(received);
    co_await t.sock.sendmsg(toSend);
  } while (t.state.getAuthenticationState() != Talos::AuthenticationState::ClientOperational &&
           t.state.getAuthenticationState() != Talos::AuthenticationState::Disconnected);
  t.alpnProtocol = t.state.getAlpnProtocol();
  co_return std::move(t);
}

Origo::future<tls> tls::createServer(Talos::TlsContextHandle& handle, tcp_socket remote, Origo::time currentTime, std::vector<std::string> supportedAlpnProtocols)
{
  tls t(handle, std::move(remote), currentTime, supportedAlpnProtocols);
  std::vector<uint8_t> received;
  received.reserve(8192);
  do {
    received.resize(8192);
    size_t bytesReceived = co_await t.sock.recvmsg(received.data(), 8192);
    if (bytesReceived == 0) 
      break;
    received.resize(bytesReceived);

    std::vector<uint8_t> toSend;
    toSend = t.state.startupExchange(received);
    co_await t.sock.sendmsg(toSend);
  } while (t.state.getAuthenticationState() != Talos::AuthenticationState::ServerOperational &&
           t.state.getAuthenticationState() != Talos::AuthenticationState::Disconnected);
  t.alpnProtocol = t.state.getAlpnProtocol();
  co_return std::move(t);
}

bool tls::isDisconnected() {
  return state.getAuthenticationState() == Talos::AuthenticationState::Disconnected;
}

Origo::future<std::vector<uint8_t>> tls::recvmsg() {
  if (state.getAuthenticationState() == Talos::AuthenticationState::Disconnected) {
    throw std::runtime_error("TLS error " + to_string(state.getError()));
  }
  std::vector<uint8_t> receivebuffer;
  while (state.getAuthenticationState() == Talos::AuthenticationState::ClientOperational ||
         state.getAuthenticationState() == Talos::AuthenticationState::ServerOperational) {
    std::vector<uint8_t> data = state.receive_decode(receivebuffer);
    if (not data.empty()) {
      co_return data;
    }
    receivebuffer.resize(8192);
    size_t bytesReceived = co_await sock.recvmsg(receivebuffer.data(), 8192);
    receivebuffer.resize(bytesReceived);
  }
  co_return {};
}

Origo::future<void> tls::sendmsg(std::span<const uint8_t> msg) {
  if (state.getAuthenticationState() == Talos::AuthenticationState::Disconnected) {
    throw std::runtime_error("TLS error " + to_string(state.getError()));
  }
  co_await sock.sendmsg(state.send_encode(msg));
}

std::string_view tls::GetAlpnProtocol() { 
  return alpnProtocol;
}

tls::tls(Talos::TlsContextHandle& handle, tcp_socket sock, std::string hostname, Origo::time currentTime, std::vector<std::string> requestedAlpnProtocols)
: sock(std::move(sock))
, state(Talos::TlsStateHandle::createClient(handle, hostname, currentTime, std::move(requestedAlpnProtocols)))
{}

tls::tls(Talos::TlsContextHandle& handle, tcp_socket sock, Origo::time currentTime, std::vector<std::string> supportedAlpnProtocols)
: sock(std::move(sock))
, state(Talos::TlsStateHandle::createServer(handle, currentTime, std::move(supportedAlpnProtocols)))
{}

}


