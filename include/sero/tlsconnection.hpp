#pragma once

#include <talos/Tls.hpp>
#include <manto/tcp_socket.hpp>
#include <cstring>
#include <map>
#include <origo/future.hpp>
#include <origo/time.hpp>

namespace Sero {

struct tls {
public:
  // Wrap a given remote socket into a secure socket (client role, the side that connect()'s)
  // handle: contains certificates and keys to authenticate with, and the CAs to trust for the remote side
  // remote: connection to the other side
  // hostname: used for SNI (multiple hostnames on the same IP address). When empty will skip SNI header.
  // currentTime: used to validate the remote certificate. Pass in from a reliable source for normal operation or a fixed value for testing.
  // requestedAlpnProtocols: when empty, a http1.1 connection will be made. Use this to request other protocols (in order of preference).
  static Origo::future<tls> createClient(Talos::TlsContextHandle& handle, tcp_socket remote, std::string hostname, Origo::time currentTime, std::vector<std::string> requestedAlpnProtocols);
  // Wrap a given remote socket into a secure socket (server role, the side that accept()'s)
  // handle: contains certificates and keys to authenticate with, and the CAs to trust for the remote side
  // remote: connection to the other side
  // currentTime: used to validate the remote certificate. Pass in from a reliable source for normal operation or a fixed value for testing.
  // requestedAlpnProtocols: when empty, a http1.1 connection will be made. Use this to request other protocols (in order of preference).
  static Origo::future<tls> createServer(Talos::TlsContextHandle& handle, tcp_socket remote, Origo::time currentTime, std::vector<std::string> supportedAlpnProtocols);
  // Indicates whether both the underlying socket is still connected (in its opinion), and the TLS connection has not been terminated.
  bool isDisconnected();
  // Receive an arbitrary non-zero-sized chunk of data. Boundaries are not kept.
  Origo::future<std::vector<uint8_t>> recvmsg();
  // Send an arbitrary amount of data.
  Origo::future<void> sendmsg(std::span<const uint8_t> msg);
  // Returns the negotiated alpn protocol. If none was requested it will return "http/1.1" (the default). 
  std::string_view GetAlpnProtocol();
private:
  tls(Talos::TlsContextHandle& handle, tcp_socket sock, std::string hostname, Origo::time currentTime, std::vector<std::string> requestedAlpnProtocols);
  tls(Talos::TlsContextHandle& handle, tcp_socket sock, Origo::time currentTime, std::vector<std::string> supportedAlpnProtocols);
  tcp_socket sock;
  Talos::TlsStateHandle state;
  std::string alpnProtocol;
};

class tlsserver {
public:
  // Retrieves the one instance of the TLS server for this process. A local one can be made instead of this, but most setups will want everything to be accessible on port 443.
  static tlsserver& Instance();
  // Adds a certificate and private key to identify with. The hostnames (for SNI use) are extracted from the certificate's CN and SAN fields.
  void addcert(std::string_view cert, std::string_view pkey);
  // Add a port to be handled by this process. The protocol specified is the default for the given port (but all registered protocols can be requested on all ports)
  void open_port(uint16_t port, std::string_view protocol);
  // Registers a handler for a given protocol. The callback receives only established connections that already negotiated the given protocol.
  void RegisterHandler(std::string protocol, std::function<void(tls, uint16_t, std::string)> onConnect);
private:
  Origo::future<void> NewConnection(tcp_socket sock, uint16_t, std::string);
  std::map<std::string, std::function<void(tls, uint16_t, std::string)>> onConnects;
  std::vector<std::unique_ptr<tcp_listen_socket>> listensockets;
  Talos::TlsContextHandle handle;
  std::map<uint16_t, std::string> defaultProtocols;
};

}


